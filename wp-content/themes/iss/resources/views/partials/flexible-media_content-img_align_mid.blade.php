@if ( $title || $content )
	<div class="media-content__container media-content__img_mid__container block__container">
		<div class="container-fluid rc-container-fluid">
			<div class="row row-two-col">
				@if ( $title || $content )
					<div class="col-12 col-md-6">
						<h3 class="media-content__title media-content__img_mid__title h1 d-flex flex-column">{{ $title }}</h3>
						<div class="media-content__content media-content__img_mid__content">
							{!! $content !!}
						</div>
					</div> <!-- / .col-12 -->
				@endif
				@if ( $img )
					<div class="col-12 col-md-6 d-flex media-content__img_mid__img">
						<img src="{!! $img['sizes']['medium'] !!}" title="{!! $img['title'] !!}" alt="{!! $img['alt'] !!}">
					</div> <!-- / .col-12 -->
				@endif
			</div> <!-- / .row -->
		</div> <!-- / .container-fluid -->
	</div> <!-- / .media-content__container -->
@endif