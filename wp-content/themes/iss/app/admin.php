<?php

namespace App;

/**
 * Theme customizer
 */
add_action('customize_register', function (\WP_Customize_Manager $wp_customize) {
  // Add postMessage support
  $wp_customize->get_setting('blogname')->transport = 'postMessage';
  $wp_customize->selective_refresh->add_partial('blogname', [
    'selector' => '.brand',
    'render_callback' => function () {
      bloginfo('name');
    }
  ]);
});

/**
 * Customizer JS
 */
add_action('customize_preview_init', function () {
  wp_enqueue_script('sage/customizer.js', asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
});

/**
 * Disable Gutenberg editor
 */
add_filter('use_block_editor_for_post_type', '__return_false', 10);

/*
 * Add ACF Options page
 */ 
add_action('acf/init', function() {
  if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
      'page_title' => __('Site Options'),
      'menu_title' => __('Site Options'),
    ));
  }
});

/**
 * ACF save json folder
 */
add_filter('acf/settings/save_json', function( $path ) {
  // update path
  $path = get_stylesheet_directory() . '/acf-json';
  // return
  return $path;
});

/**
 * ACF Load json folder
 */
add_filter('acf/settings/load_json', function($paths) {
    // remove original path (optional)
    unset($paths[0]);
    
    // append path
    $paths[] = get_stylesheet_directory() . '/acf-json';
    
    // return
    return $paths;
});

/**
 * Inject ACF content in to the_content()
 * Save ACF image as the_post_thumbnail
 *
 * @param int  $post_id          IDs of the current post
 * @return
 */
add_action('acf/save_post', __NAMESPACE__ .'\\save_acf_as_content', 20);
function save_acf_as_content( $post_id )
{
  // If saving changes to a page in the admin
  if( is_admin() ) {
    if( get_current_screen()->post_type === 'page' ) {
      // Get ACF field data
      $title_top     = get_field('content_title_top', $post_id, false);
      $title_center  = get_field('content_title_center', $post_id, false);
      $title_bottom  = get_field('content_title_bottom', $post_id, false);
      $title         = $title_top .' '. $title_center .' '. $title_bottom;
      $content       = get_field('content_area', $post_id, false);
      
      // Save ACF content to WP content so SEO plugins work as expected
      if ( $title && $content ) {
        $post_content = '<h2>'. $title .'</h2><div>'. $content .'</div>';
        wp_update_post([ 'ID' => $post_id, 'post_content' => $post_content]);
      } elseif ( $title ) {
        $post_content = '<h2>'. $title .'</h2>';
        wp_update_post([ 'ID' => $post_id, 'post_content' => $post_content]);
      } elseif ( $content ) {
        $post_content = '<div>'. $content .'</div>';
        wp_update_post([ 'ID' => $post_id, 'post_content' => $post_content]);
      }
    // If saving changes a course
    } elseif( get_current_screen()->post_type === 'courses' ) {
      // Get ACF field data
      $has_thumbnail = get_the_post_thumbnail( $post_id );
      $image         = get_field('featured_image', $post_id, false);
      // If ACF image is different to WP featured image, overwrite
      if ( !$has_thumbnail || $image != get_post_thumbnail_id() ) {
        $image_id = $image;
        if ( $image_id ) {
          set_post_thumbnail( $post_id, $image_id );
        } else {
          // If no ACF image exists, remove WP featured image
          delete_post_thumbnail( $post_id );
        }
      }
      // Get ACF field data
      $content       = get_field('course_content', $post_id, false);
      $objectives    = get_field('course_objectives', $post_id, false);

      // Save ACF content to WP content so SEO plugins work as expected
      if ( $content || $objectives ) {
        $post_content = '<div>'. $content .'</div>' . '<div>'. $objectives .'</div>';
        wp_update_post([ 'ID' => $post_id, 'post_content' => $post_content]);
      }
    }
  }
  return;
}
