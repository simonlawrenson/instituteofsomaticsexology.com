@if ( $header_title || $header_subtitle )
	<div class="header__container block__container">
		<div class="container-fluid rc-container-fluid">
			<div class="row">
				<div class="col-12 header__subtitle-container d-flex flex-column" id="triggerLogoAnimation">
					{!! '<h2 class="header__subtitle">'. $header_subtitle .'</h2>' !!}</h2>
					<div class="animate__logo-container header__logo-container">
						<img src="@asset('images/logos/logo-pink-loop.svg')" alt="{{ $site_name .' Logo'}}" title="{{ $site_name }}" class="header__logo spin-logo animate__logo-ss">
						<img src="@asset('images/logos/animation/logo-step-1.svg')" alt="{{ $site_name .' Logo'}}" title="{{ $site_name }}" class="header__logo animate__logo animate__logo-1">
						<img src="@asset('images/logos/animation/logo-step-2.svg')" alt="{{ $site_name .' Logo'}}" title="{{ $site_name }}" class="header__logo animate__logo animate__logo-2">
						<img src="@asset('images/logos/animation/logo-step-3.svg')" alt="{{ $site_name .' Logo'}}" title="{{ $site_name }}" class="header__logo animate__logo animate__logo-3">
						<img src="@asset('images/logos/animation/logo-step-3.svg')" alt="{{ $site_name .' Logo'}}" title="{{ $site_name }}" class="header__logo animate__logo animate__logo-3 animate__logo-4">
						<img src="@asset('images/logos/animation/logo-step-3.svg')" alt="{{ $site_name .' Logo'}}" title="{{ $site_name }}" class="header__logo animate__logo animate__logo-3 animate__logo-5">
					</div> <!-- / .navigation__logo-container -->
				</div> <!-- / .header__subtitle-container -->

				@if ( $header_title )
					<div class="col-12 header__title-container">
						<div class="header__title">{!! $header_title !!}</div>
					</div> <!-- / .header__title-container -->
				@endif
			</div> <!-- / .row -->
		</div> <!-- / .container-fluid -->
		<div id="triggerNav"></div> <!-- / ScrollMagic Trigger -->
	</div> <!-- / .header__container -->
@endif

@if ( $title_top || $title_center || $title_bottom || $content)
	<div class="content__container block__container background__{{ $bg_color }}">
		<div class="container-fluid rc-container-fluid">
			<div class="row row row-two-col align-items-center">
				@if ( $title_top || $title_center || $title_bottom )
					<div class="col-12 col-sm-auto ml-auto mr-auto">
						<h3 class="content__title h1 d-md-flex flex-column">
							<span class="content__title-top">{{ $title_top }}</span>
							<span class="content__title-center">{{ $title_center }}</span>
							<span class="content__title-bottom">{{ $title_bottom }}</span>
						</h3>
					</div> <!-- / .col-12 -->
				@endif
				@if ( $content )
					<div class="col-12 col-lg content__area">
						{!! $content !!}
					</div> <!-- / .col-12 -->
				@endif
			</div> <!-- / .row -->
		</div> <!-- / .container-fluid -->
	</div> <!-- / .content-container -->
@endif