@extends('layouts.app')

@section('content')
<div class="single__container single-courses__container">
	@while(have_posts()) @php the_post() @endphp
		<div class="container-fluid">
			<div class="row">
				<div class="col-12 col-md-6 bordered-box__container color__{!! get_field('course_color') !!}">
					<!-- Load ACF Course data -->
			    @include('partials.course-card-boxed', [
			      'title'   			=> get_the_title(),
			      'location'   		=> get_field('location'),
			      'start_date' 		=> get_field('start_date'),
			      'end_date'  		=> get_field('end_date'),
			      'text_color'  	=> get_field('text_color'),
			      'link'  				=> get_the_permalink(),
			    ])
				</div> <!-- / .col-12 -->
				<div class="col-12 col-md-6 course__info-container">
		    	@include('partials.content-single-'. get_post_type())
				</div> <!-- / .col-12 -->
			</div> <!-- / .row -->
			<div class="row location__row align-items-lg-center">
		  	{!! $locations !!}
			</div> <!-- / .row -->
		</div> <!-- / .container-fluid -->
	@endwhile
</div> <!-- / .archive__container -->
@endsection