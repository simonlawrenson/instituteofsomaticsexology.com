<footer class="content-info footer">
  <div class="container-fluid rc-container-fluid">
  	<div class="row row-two-col align-items-center">
  		<div class="d-none d-md-block col-md-4">
  			<div class="footer__logo-container spin-logo-container">
  				<img src="@asset('images/logos/logo-pink-loop.svg')" alt="{{ $site_name .' Logo'}}" title="{{ $site_name }}" class="footer__logo spin-logo">
		 		</div> <!-- / .footer__logo-container -->

		 	</div> <!-- / .col-4 -->
	  	@if( $contact_email || $contact_tel || $contact_tel_alt )
				<div class="col-12 col-md-8 footer__contact">
	  			@if( $contact_email )
						<p>For more information or to arrange a time to speak with us, please contact: <a href="{{ $contact_email['click'] }}" title="Email us">{{ $contact_email['display'] }}</a></p>
	  			@endif
	  			@if( $contact_tel )
						<p><a href="{{ $contact_tel['click'] }}" title="Call us">{{ $contact_tel['display'] }}</a></p>
	  			@endif
	  			@if( $contact_tel_alt )
						<p><a href="{{ $contact_tel_alt['click'] }}" title="Call us">{{ $contact_tel_alt['display'] }}</a></p>
	  			@endif
	  			@if( $contact_mailing_list )
						{!! do_shortcode( $contact_mailing_list) !!}
	  			@endif
  			</div> <!-- / .col-12 -->
	  	@endif
  	</div> <!-- / .row -->
  	@if (has_nav_menu('footer_navigation'))
			<div class="row">
				<div class="col-12">
					<nav class="nav-secondary" role="navigation">
				    {!! wp_nav_menu([
				      'theme_location'  => 'footer_navigation',
				      'container'       => '',
				      'menu_class'      => 'nav nav-fill',
				      'depth'           => 2,
				      'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
				      'walker'          => new WP_Bootstrap_Navwalker(),
				    ]) !!}
			  	</nav>
	 			</div> <!-- / .col-12 -->			
	 		</div> <!-- / .row -->			
		@endif
  </div> <!-- / .container-fluid -->
</footer>