import {TweenMax, Linear, TimelineMax} from 'gsap/all';
// import	 { Linear } from 'gsap/all';
import ScrollMagic from 'ScrollMagic';
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';

export default {
  init() {
    // JavaScript to be fired on library template pages
  },
  finalize() {
    // JavaScript to be fired on library template pages, after page specific JS is fired
    // Animate Coffees.svg using ScrollMagic
    function pathPrepare ($el) {
			var lineLength = $el[0].getTotalLength();
			$el.css('stroke-dasharray', lineLength);
			$el.css('stroke-dashoffset', lineLength);
		}

		var $coffee = $('.library-svg path');

		var halfLength = $coffee[0].getTotalLength() / 2 + 15;
		// var halfLength = 0;

		// prepare SVG
		pathPrepare($coffee);

		// init controller
		var controller = new ScrollMagic.Controller();
		// build tween
		var tween = new TimelineMax()
			.add(TweenMax.to($coffee, 4, {
				strokeDashoffset: halfLength,
				stroke: '#000000',
				ease:Linear.easeNone,
			})); // draw word for 1

		// build scene
		new ScrollMagic.Scene({
			triggerElement: '.wrap',
			duration: 200,
			reverse: false,
			offset: -500,
			tweenChanges: true,
		})
		.setTween(tween)
		.addTo(controller);
  },
};
