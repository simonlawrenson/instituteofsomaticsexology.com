{{--
  Template Name: Courses Template
--}}

@extends('layouts.app')

@section('content')
<div class="archive__container archive-courses__container">
  @while(have_posts()) @php the_post() @endphp
     <!-- Load ACF Course data -->
    @include('partials.archive-course', [
      'title'   			=> get_the_title(),
      'location'   		=> get_sub_field('layout_type'),
      'start_date' 		=> get_sub_field('media_content_title_left'),
      'end_date'  		=> get_sub_field('media_content_title_center'),
      'link'  				=> get_the_permalink(),
    ])
  @endwhile
</div> <!-- / .archive-container -->
@endsection
