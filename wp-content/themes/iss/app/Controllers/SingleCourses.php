<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class SingleCourses extends Controller
{
  /**
   * Populate and return the locations template(s) on the courses page
   */
  public function locations() {

    // Initialize the return variable
    $return = '';

    // Get the appropriate projects
    $locations = get_field('locations');

    if(is_array($locations)) {
      $iteration = 0;
      foreach( $locations as $l ) {

        $return .= \App\template('partials.course-location', [
          'count'   => count($locations),
          'id'      => ++$iteration,
          'name'    => $l['location_name'],
          'icon'    => 'images/locations/'. $l['location_icon'] .'.svg',
          'color'   => $l['location_color'],
          'year'    => $l['location_year'],
          'modules' => $l['course_modules'],
          'fees'    => $l['course_fees'],
          ]
        );

      }
    }

    // Always return
    return $return;
  }

  /**
   * Populate and return the locations template(s) on the courses page
   */
  public function course_download_shortcode()
  {
    $course_download_shortcode = get_field('course_download_shortcode', 'options');
    if ($course_download_shortcode) {
      return $course_download_shortcode;
    }
    return false;
  }
}
