<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class App extends Controller
{
  public function siteName()
  {
      return get_bloginfo('name');
  }

  public static function title()
  {
    if (is_home()) {
      if ($home = get_option('page_for_posts', true)) {
        return get_the_title($home);
      }
      return __('Latest Posts', 'iss');
    }
    if (is_archive()) {
      return get_the_archive_title();
    }
    if (is_search()) {
      return sprintf(__('Search Results for %s', 'iss'), get_search_query());
    }
    if (is_404()) {
      return __('Not Found', 'iss');
    }
    return get_the_title();
  }

  /**
   * Get email from ACF Options
   *
   * @return array $email  Array containing display and mailto: links
   */
  public function contactEmail()
  {
    $contact_email = get_field('contact_email', 'options');
    if ($contact_email) {
      $email = [
        'display' => $contact_email,
        'click' => 'mailto:'. $contact_email,
      ];
      return $email;
    }
    return false;
  }
  
  /**
   * Get Phone number from ACF Options
   *
   * @return array $phone  Array containing display and tel: number
   */
  public function contactTel()
  {
    $contact_tel = get_field('contact_tel', 'options');
    if ($contact_tel) {
      $phone = [
        'display' => $contact_tel,
        'click' => 'tel:'. \App\clean_phone($contact_tel),
      ];
      return $phone;
    }
    return false;
  }
  
  /**
   * Get Alternative Phone number from ACF Options
   *
   * @return array $phone_alt  Array containing display and tel: number
   */
  public function contactTelAlt()
  {
    $contact_tel_alt = get_field('contact_tel_alt', 'options');
    if ($contact_tel_alt) {
      $phone_alt = [
        'display' => $contact_tel_alt,
        'click' => 'tel:'. \App\clean_phone($contact_tel_alt),
      ];
      return $phone_alt;
    }
    return false;
  }
  
  /**
   * Get form shortcode (mailing list) from ACF Options
   *
   * @return string $contact_mailing_list  Gravity Forms shortcode
   */
  public function contactMailingList()
  {
    $contact_mailing_list = get_field('mail_list_shortcode', 'options');
    if ($contact_mailing_list) {
      return $contact_mailing_list;
    }
    return false;
  }
}
