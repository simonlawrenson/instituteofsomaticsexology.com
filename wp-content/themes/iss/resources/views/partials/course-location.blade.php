<div class="col-12 col-lg location__container location__container__{!! $color !!} @if( $count === 1 ) location__active @endif">
	<div class="row no-gutters align-items-lg-center">
		<div class="col-12 col-lg location__content">
			<a href="javascript:void(0)" class="location__link">
				@if( $name )
					<h4 class="location__title">{!! $name !!}</h4>
				@endif
				@if( $icon )
					<div class="location__icon">
						<img src="@asset($icon)" title="" alt="">
					</div>
				@endif
				@if( $year )
					<p class="location__year">{!! $year !!}</p>
				@endif
			</a>
		</div> <!-- /.col-12 -->

		<!-- Course location information -->
		<div class="col-12 col-lg-6 course-details__container">
			@if( $modules || $fees )
				<div class="course-details__table" @if( $count === 1 )style="display: block;" @endif>
					@if( $modules )
						<div class="table__header">
							<p>Course Dates for {!! $name !!} {!! $year !!}</p>
						</div> <!-- / .table__header -->
						<div class="table__body module__row d-sm-flex">
							@foreach( $modules as $m )
								<div class="table__col module__col d-flex flex-column">
									<p class="module__type">Module <span>{!! $loop->iteration !!}</span></p>
									<p class="module__title">{!! $m['module_title'] !!}</p>
									<hr class="table__hr">
									<p class="module__date">{!! $m['module_date'] !!}</p>
								</div> <!-- / .table__col -->
							@endforeach
						</div> <!-- / .table__body -->
					@endif
					@if( $fees )
						<div class="table__header">
							<p>Course Fees for {!! $name !!} {!! $year !!}</p>
						</div> <!-- / .table__header -->
						<div class="table__body fees__row d-sm-flex">
							@foreach( $fees as $f )
								<div class="table__col fees__col d-flex flex-column">
									<p class="fees__type">{!! $f['fee_title'] !!}</p>
									<p class="fees__date">{!! $f['fee'] !!}</p>
									<hr class="table__hr">
									<p class="fees__tcs">{!! $f['fee_tcs'] !!}</p>
								</div> <!-- / .table__col -->
							@endforeach
						</div> <!-- / .table__body -->
					@endif
				</div> <!-- /.course-details__table -->
			@endif
		</div> <!-- /.course-details__container -->
	</div> <!-- /.row -->
</div> <!-- /.location__container -->