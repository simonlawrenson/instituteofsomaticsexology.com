<div class="row">
	<!-- Featured image layout: left col -->
	@if( has_post_thumbnail() )
		<div class="col-12 course__info-box course__info-image">
			<div class="course__info-featured-img" style="background-image:url({!! $featured_img !!});">
			</div> <!-- / .course__info-feature-img -->
		</div> <!-- / .col-12 -->
	@endif
	
	<!-- Content: right col -->
	<div class="col-12 course__info-box course__info-content">
		@if( $content )
			<div class="course__info-about">
				{!! $content !!}
			</div> <!-- / .course__info-content -->
		@endif
		@if( $objectives )
			<div class="course__info-objectives">
				{!! $objectives !!}
			</div> <!-- / .course__info-objectives -->
		@endif
		
		<!-- Course content more info -->
		<div class="course__info-buttons button-links__buttons d-sm-flex justify-content-sm-between">
			@if ( $download && $course_download_shortcode )
				<a href="" title="Download Full Course Description" data-toggle="modal" data-target="#modal-download" class="btn btn__offw">Full Course Description</a>
			@endif
			@if ( $contact_email )
				<a href="{{ $contact_email['click'] }}" title="Get in Touch" class="btn btn__offw">Get in Touch</a>
			@endif
		</div> <!-- / .button-links__buttons -->
	</div> <!-- / .course__info-content -->

	<!-- Course testimonials layout -->
	@if( $testimonials )
		<div class="col-12 course__info-box course__info-testimonials testimonials__slider-container">
			<div class="testimonials__slider-init">
				@foreach( $testimonials as $testimonial )
					<div class="testimonials__slide">
						<p><img src="@asset('images/speechmarks-open-03.svg')" class="testimonials__speech speech__open">{!! $testimonial->post_content !!}<img src="@asset('images/speechmarks-closed-03.svg')" class="testimonials__speech speech__closed"></p>
					</div> <!-- / .testimonials__slide -->
				@endforeach
			</div> <!-- / .testimonials__slider-container -->
		</div> <!-- / .course__info-objectives -->
	@endif
</div> <!-- / .row -->

<!-- Course download information -->
@if ( $download && $course_download_shortcode )
	<!-- Modal -->
	<div class="modal fade" id="modal-download" tabindex="-1" role="dialog" aria-labelledby="Popup Download Course Description" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title">Download Full Course Description</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	{!! do_shortcode($course_download_shortcode) !!}
	      </div>
	    </div>
	  </div>
	</div>
@endif