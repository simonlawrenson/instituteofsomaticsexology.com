@extends('layouts.app')

@section('content')

<div class="archive__container archive-courses__container">
	<div class="container-fluid">
		<div class="row">
			@while(have_posts()) @php the_post() @endphp
				<!-- Load ACF Course data -->
			  <a href="{{ get_the_permalink() }}" class="bordered-box__container color__{!! get_field('course_color') !!}">
			    @include('partials.course-card-boxed', [
			      'title'   			=> get_the_title(),
			      'location'   		=> get_field('location'),
			      'start_date' 		=> get_field('start_date'),
			      'end_date'  		=> get_field('end_date'),
			      'text_color'  	=> get_field('text_color'),
			    ])
				</a> <!-- .bordered-box -->
		  @endwhile
		</div> <!-- / .row -->
	</div> <!-- / .container-fluid -->
</div> <!-- / .archive__container -->

<!-- Load hardcoded Edu Pathway content -->
<!-- Note: Client requested this method -->
<!-- Todo: Find better solution for client in Phase 2 -->
<div class="education__container">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<h1 class="education__title">Education Pathways</h1>
				<p class="education__content">We offer a range of professional pathways to vital living, somatic learning, and embodied pleasure. As we work with practitioners from diverse backgrounds and experiences, we have designed educational pathways to be tailored to suit the individual for their optimal learning. Follow the arrows to find the path that is most useful for you and the field you are interested in. There are certain prerequisite certifications that need to be completed before moving onto the next course. We also offer stand-alone courses, professional training and supervision which do not require prior study with the Institute and may also be used as an introduction into working with us.</p>
			</div> <!-- / .col-12 -->
		</div> <!-- / .row -->

		<div class="row">
			<div class="col-12">
				<img src="@asset('images/education-pathway-flow-chart.svg')" alt="Education Pathways" class="education-pathway-flow-chart">
			</div> <!-- / .col-12 -->
		</div> <!-- / .row -->

	</div> <!-- / .container-fluid -->
</div> <!-- / .education__container -->

<!-- Load testimonials -->
@if( $testimonials )
	<div class="archive-courses__testimonials-container">	
		<div class="row">	
			<div class="col-12 col-md-8 archive-courses__testimonials">	
				<div class="testimonials__slider-container">
					@foreach( $testimonials as $testimonial )
						<div class="testimonials__slide">
							{!! $testimonial['content'] !!}
						</div> <!-- / .testimonials__slide -->
					@endforeach
				</div> <!-- / .testimonials__slider-container -->
			</div> <!-- / .archive-courses__testimonials -->
			<div class="col-12 col-md-4 d-flex align-items-end justify-content-end archive-courses__testimonials-extra">
				<img src="@asset('images/speechmarks-closed.svg')" alt="Speachmarks" title="Institute of Somatic Sexology" class="archive-courses__testimonials-img">
			</div> <!-- / .archive-courses__testimonials-extra -->
		</div> <!-- / .row -->
	</div> <!-- / .archive-courses__testimonials-container -->
@endif
@endsection