<?php

namespace App;

/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {
  /** Add page slug if it doesn't exist */
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  /** Add class if sidebar is active */
  if (display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  /** Clean up class names for custom templates */
  $classes = array_map(function ($class) {
      return preg_replace(['/-blade(-php)?$/', '/^page-template-views/'], '', $class);
  }, $classes);

  return array_filter($classes);
});

/**
 * Add "… Continued" to the excerpt
 */
add_filter('excerpt_more', function () {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'iss') . '</a>';
});

/**
 * Template Hierarchy should search for .blade.php files
 */
collect([
  'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
  'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment', 'embed'
])->map(function ($type) {
  add_filter("{$type}_template_hierarchy", __NAMESPACE__.'\\filter_templates');
});

/**
 * Render page using Blade
 */
add_filter('template_include', function ($template) {
  collect(['get_header', 'wp_head'])->each(function ($tag) {
    ob_start();
    do_action($tag);
    $output = ob_get_clean();
    remove_all_actions($tag);
    add_action($tag, function () use ($output) {
      echo $output;
    });
  });
  $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
    return apply_filters("sage/template/{$class}/data", $data, $template);
  }, []);
  if ($template) {
    echo template($template, $data);
    return get_stylesheet_directory().'/index.php';
  }
  return $template;
}, PHP_INT_MAX);

/**
 * Render comments.blade.php
 */
add_filter('comments_template', function ($comments_template) {
  $comments_template = str_replace(
    [get_stylesheet_directory(), get_template_directory()],
    '',
    $comments_template
  );

  $data = collect(get_body_class())->reduce(function ($data, $class) use ($comments_template) {
      return apply_filters("sage/template/{$class}/data", $data, $comments_template);
  }, []);

  $theme_template = locate_template(["views/{$comments_template}", $comments_template]);

  if ($theme_template) {
    echo template($theme_template, $data);
    return get_stylesheet_directory().'/index.php';
  }

  return $comments_template;
}, 100);

/**
 * Add ACF data references for Page templates.
 */
add_filter('sage/template/page/data', function (array $data) {
  $page_id = get_the_ID();

  $data['header_title']        = get_field('header_title', $page_id );
  $data['header_subtitle']     = get_field('header_subtitle', $page_id );

  $data['bg_color']      = get_field('bg_color', $page_id );
  $data['title_top']     = get_field('content_title_top', $page_id );
  $data['title_center']  = get_field('content_title_center', $page_id );
  $data['title_bottom']  = get_field('content_title_bottom', $page_id );
  $data['content']       = get_field('content_area', $page_id );

  return $data;
});

/**
 * Add data references for single Course templates.
 */
add_filter('sage/template/single/data', function (array $data) {
  $page_id = get_the_ID();
  $data['featured_img'] = get_the_post_thumbnail_url($page_id, 'course-featured-image');
  $data['content']      = get_field('course_content', $page_id);
  $data['objectives']   = get_field('course_objectives', $page_id);
  $data['download']     = get_field('course_description', $page_id);
  $data['testimonials'] = get_field('testimonials', $page_id);

  return $data;
});

/**
 * Order courses in th main query
 */
add_filter('pre_get_posts', function (object $query) {
  if( !is_admin() && $query->is_main_query() && is_post_type_archive('courses') ) :
      $query->set('orderby', 'menu_order');
      $query->set('order', 'DESC');
  endif;
  return;
});

/*
 * Add GTM code to head
 * @hook WordPress wp_head
 */
add_action('wp_head', function () {

  if( $_SERVER['HTTP_HOST'] === 'instituteofsomaticsexology.com' || $_SERVER['HTTP_HOST'] === 'www.instituteofsomaticsexology.com' ) : ?>
    <!-- Google Tag Manager -->
    <script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-K5K97KM');
    </script>
    <!-- End Google Tag Manager -->
<?php endif;
}, -10);

/*
 * Add GTM code to body
 * @hook Custom before_header
 */
add_action('before_header', function () {

  if( $_SERVER['HTTP_HOST'] === 'instituteofsomaticsexology.com' || $_SERVER['HTTP_HOST'] === 'www.instituteofsomaticsexology.com' ) : ?>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K5K97KM"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
  <?php endif;
}, 0);
