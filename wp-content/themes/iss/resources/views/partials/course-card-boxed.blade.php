<div class="bordered-box__container-inner d-flex flex-column">
	<div class="bordered-box__header">
		@if ($start_date && $end_date)
			<p>{!! $start_date !!} - {!! $end_date !!}</p>
		@elseif ($start_date)
			<p>{!! $start_date !!}</p>
		@elseif ($end_date)
			<p>{!! $end_date !!}</p>
		@else
			<p class="empty"></p>
		@endif
	</div> <!-- .bordered-box__header -->
	<div class="bordered-box__body">
		@if ($title)
			<p>{!! $title !!}</p>
		@endif
		@if ($location)
			<p>{!! $location !!}</p>
		@endif
	</div> <!-- .bordered-box__body -->
</div> <!-- .bordered-box__container -->