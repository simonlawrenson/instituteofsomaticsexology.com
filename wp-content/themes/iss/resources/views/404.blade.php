@extends('layouts.app')

@section('content')
<div class="not-found__container block__container">
	<div class="container-fluid rc-container-fluid">
		<div class="row">
			<div class="col-12">
  			@include('partials.page-header')
			  @if (!have_posts())
			    <div class="alert alert-warning">
			      {{ __('Sorry, but the page you were trying to view does not exist.', 'iss') }}
			    </div>
			  @endif
			</div> <!-- / .col-12 -->
		</div> <!-- / .row -->
	</div> <!-- / .container-fluid -->
</div> <!-- / .not-found__container -->

@endsection
