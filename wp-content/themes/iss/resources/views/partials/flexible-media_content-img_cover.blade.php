@if ( $img )
	<style type="text/css">
		.media-content__img_cover__container::before  {
			background-image: url({{ $img['url']  }});
		}
	</style>
@endif
@if ( $title_left || $title_center || $title_right || $content )
	<div class="media-content__container media-content__img_cover__container block__container">
		<div class="container-fluid">
			<div class="row">
				@if ( $title_left || $title_center || $title_right )
					<div class="media-content__title_container media-content__img_cover__title_container col-12 col-md-6 d-flex justify-content-end align-items-lg-end">
						<h3 class="media-content__title media-content__img_cover__title h1 d-md-flex flex-column">
							<span class="media-content__img_cover__title-top">{{ $title_left }}</span>
							<span class="media-content__img_cover__title-center">{{ $title_center }}</span>
							<span class="media-content__img_cover__title-bottom">{{ $title_right }}</span>
						</h3>
					</div> <!-- / .col-12 -->
				@endif
				@if ( $content )
					<div class="media-content__content media-content__img_cover__content col-12 col-md-6">
						{!! $content !!}
					</div> <!-- / .col-12 -->
				@endif
			</div> <!-- / .row -->
		</div> <!-- / .container-fluid -->
	</div> <!-- / .media-content__container -->
@endif