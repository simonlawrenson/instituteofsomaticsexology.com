export default {
  init() {
    // JavaScript to be fired on the archive courses template
  },
  finalize() {
    // JavaScript to be fired on the archive courses template, after page specific JS is fired
    // JavaScript to be fired when slider is active
    const testimonial_slider = $('.testimonials__slider-container');
    const testimonial_settings = {
      adaptiveHeight: true,
      arrows: false,
      appendDots: $('.testimonials__slider-container'),
      autoplay: false,
      dots: true,
      infinite: true,
      slidesToScroll: 1,
      slidesToShow: 1,
      speed: 500,
      variableWidth: false,
      responsive: [
        {
          breakpoint: 200,
          settings: 'unslick',
        },
      ],
    };
    testimonial_slider.slick(testimonial_settings);

    // reslick only if it's not slick()
    $(window).on('resize', function() {
      if ($(window).width() < 200) {
        if (testimonial_slider.hasClass('slick-initialized')) {
          testimonial_slider.slick('unslick');
        }
        return;
      }
      if (!testimonial_slider.hasClass('slick-initialized')) {
        return testimonial_slider.slick(testimonial_settings);
      } 
    });
  },
};
