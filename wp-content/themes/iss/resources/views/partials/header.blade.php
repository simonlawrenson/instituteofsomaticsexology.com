<header class="banner sticky-top">
  <div class="container-fluid">
  	<div class="row justify-content-center align-items-center">
  		<div class="col">
  			<div class="banner__site_name">
	    		<a class="banner__brand" href="{{ home_url('/') }}">{{ get_bloginfo('name', 'display') }}</a>
	        <button class="navbar-opener navbar-toggler" type="button" data-toggle="offcanvas" data-target="#navigation" aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation">
	        	<span></span>
	        </button>
  			</div> <!-- / .banner__site_name -->
  		</div> <!-- / .col -->
  	</div> <!-- / .row -->
  </div> <!-- / .container-fluid -->
	@if (has_nav_menu('primary_navigation'))
		<div id="navigation" class="navigation__container d-flex justify-content-between flex-column">

			@if( $site_name || $contact_email || $contact_tel || $contact_tel_alt )
				<div class="navigation__contact">
					<div class="d-flex justify-content-between">
		  			@if( $contact_email )
							<a href="{{ esc_url(home_url() ) }}" title="{{ $site_name }}" class="navigation__link navigation__link__home">{{ $site_name }}</a>
		  			@endif
		  			@if( $contact_email )
							<a href="{{ $contact_email['click'] }}" title="Email us" class="navigation__link">{{ $contact_email['display'] }}</a>
		  			@endif
		  			@if( $contact_tel )
							<a href="{{ $contact_tel['click'] }}" title="Call us" class="navigation__link">{{ $contact_tel['display'] }}</a>
		  			@endif
		  			@if( $contact_tel_alt )
							<a href="{{ $contact_tel_alt['click'] }}" title="Call us" class="navigation__link">{{ $contact_tel_alt['display'] }}</a>
		  			@endif
		  		</div> <!-- / .navigation__contact -->
		  	</div> <!-- / .navigation__contact -->
		  @endif
	 		

			<div class="navigation__menu">
				<nav class="nav-primary" role="navigation">
			    {!! wp_nav_menu([
			      'theme_location'  => 'primary_navigation',
			      'container'       => '',
			      'menu_class'      => 'navigation__nav nav flex-column',
			      'depth'           => 2,
			      'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
			      'walker'          => new WP_Bootstrap_Navwalker(),
			    ]) !!}
		  	</nav>
	 		</div> <!-- / .navigation__menu -->			

	 		<div class="navigation__footer">
	 			<div class="spin-logo-container navigation__logo-container">
					<img src="@asset('images/logos/logo-red-loop.svg')" alt="{{ $site_name .' Logo'}}" title="{{ $site_name }}" class="navigation__logo spin-logo">
		 		</div> <!-- / .navigation__logo-container -->
		 	</div> <!-- / .navigation__footer -->

		</div> <!-- / .navigation__container -->
	@endif
</header>