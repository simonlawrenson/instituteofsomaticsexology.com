@if ( $title || $content || $img )
	<div class="media-content__container media-content__img_align_bot__container block__container">
		<div class="container-fluid rc-container-fluid">
			<div class="row row-two-col">
				@if ( $img || $content )
					@if ( $img )
						<div class="col-12 col-md-6 d-flex media-content__img_align_bot__img">
							<img src="{{ $img['sizes']['media-content-bottom'] }}" alt="" title="" />
						</div> <!-- / .col-12 -->
					@endif
					@if ( $title || $content )
						<div class="col-12 col-md-6 media-content__img_align_bot__content-container d-flex flex-column justify-content-center">
							@if ( $title )
								<h3 class="media-content__title media-content__img_align_bot__title h1 d-flex flex-column">{{ $title }}</h3>
							@endif
							@if ( $content )
								<div class="media-content__img_align_bot__content">
									{!! $content !!}
								</div> <!-- / .media-content__img_align_bot__content -->
							@endif
						</div> <!-- / .col-12 -->
					@endif
				@endif
			</div> <!-- / .row -->
		</div> <!-- / .container-fluid -->
	</div> <!-- / .media-content__container -->
@endif