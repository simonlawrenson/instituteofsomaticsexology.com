import { Linear, TimelineMax } from 'gsap/all';
import ScrollMagic from 'ScrollMagic';
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';

export default {
  init() {
    // JavaScript to be fired on Faculty template pages
  },
  finalize() {
    // JavaScript to be fired on Faculty template pages, after page specific JS is fired
    // init controller, set varibles, get initial window dimensions
    let tooSmall   = false;
    let controller = null;
    let wWidth     = $(window).width();
    let wHeight    = $(window).height();
    const maxWidth   = 992; // or whatever your max width is
    const maxHeight   = 700; // or whatever your max width is
    
    if( wWidth < maxWidth || wHeight < maxHeight ) {
      tooSmall = true;
    }
    
    function initScrollMagic() {
      controller = new ScrollMagic.Controller();
      // get all panels on page
      const wipePanelsContainer = document.querySelectorAll('.fixed-scroller__panel__container');
      let total, totalCols, wipeAnimation, wipePanels, i, i_2, iClass = 1, i_2Class = 1;
      totalCols = wipePanelsContainer.length;
      for (i = 1; i <= totalCols; i++) {
        wipePanels = 0;
        wipeAnimation         = new TimelineMax(),
        wipePanels            = document.querySelectorAll('.fixed-scroller__panel__container-' + iClass + ' .fixed-scroller__panel'),
          total = wipePanels.length;
        for (i_2 = 1 ; i_2 <= total; ++i_2) {
          if( i_2Class % total == 1 ) {
            wipeAnimation.fromTo('.fixed-scroller__panel__container-' + iClass + ' .fixed-scroller__img-container-' + i_2Class, 1, {y: '0%'}, {y: '0%', ease: Linear.easeNone}) // in from bottom
          } else {
            if( iClass % 2 == 0 ) {
              wipeAnimation.fromTo('.fixed-scroller__panel__container-' + iClass + ' .fixed-scroller__img-container-' + i_2Class, 1, {y: '-100%'}, {y: '0%', ease: Linear.easeNone}) // in from top
            } else {
              wipeAnimation.fromTo('.fixed-scroller__panel__container-' + iClass + ' .fixed-scroller__img-container-' + i_2Class, 1, {y: '100%'}, {y: '0%', ease: Linear.easeNone}) // in from bottom
            }
          }
          i_2Class++;
          if( iClass % 2 == 0 ) {
            wipeAnimation.fromTo('.fixed-scroller__panel__container-' + iClass + ' .fixed-scroller__content-container-' + i_2Class, 1, {y: '-100%'}, {y: '0%', ease: Linear.easeNone}) // in from top
          } else {
            wipeAnimation.fromTo('.fixed-scroller__panel__container-' + iClass + ' .fixed-scroller__content-container-' + i_2Class, 1, {y: '100%'}, {y: '0%', ease: Linear.easeNone}) // in from bottom
          }
        }
        // iClass++;
        // create scene to pin and link animation
        new ScrollMagic.Scene({
            triggerElement: '.wrap',
            triggerHook: 'onLeave',
            offset: -80,
            duration: '100%',
          })
          .setPin('.fixed-scroller__panel__container-' + iClass++)
          .setTween(wipeAnimation)
          // .addIndicators() // add indicators (requires plugin)
          .addTo(controller);// create scene to pin and link animation
      }
    }

    $.fn.isOnScreen = function() {
      const win = $(window);

      const viewport = {
        top : win.scrollTop(),
        left : win.scrollLeft(),
      };
      viewport.right = viewport.left + win.width();
      viewport.bottom = viewport.top + win.height();

      const bounds = this.offset();
      bounds.right = bounds.left + this.outerWidth();
      bounds.bottom = bounds.top + this.outerHeight();

      return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
    };

    if( !tooSmall ) {
      initScrollMagic();
    }

    // part of the problem is that window resizes can trigger multiple times as the events fire rapidly
    // this solution prevents the controller from being initialized/destroyed more than once
    $(window).resize( function() {
      wWidth  = $(window).width();
      wHeight = $(window).height();
      if( wWidth < maxWidth || wHeight < maxHeight ) {
        if( controller !== null && controller !== undefined ) {
          // completely destroy the controller
          // if needed, use jQuery to manually remove styles added to DOM elements by GSAP etc. here
          controller = controller.destroy( true );
        }
      } else if( wWidth >= maxWidth || wHeight >= maxHeight ) {
        if( controller === null || controller === undefined ) {
          // reinitialize ScrollMagic only if it is not already initialized
          initScrollMagic();
        } else {
          controller = controller.destroy( true );
          initScrollMagic();
        }
      }
    });
    
    // Add a class once the scroll animation has finished
    $(window).scroll(function(){
      if ($('.content-info').isOnScreen()) {
        // The element is visible, do something
        $('.fixed-scroller__container__inner').addClass('footer-reached');
      } else {
        // The element is NOT visible, do something else
        $('.fixed-scroller__container__inner').removeClass('footer-reached');
      }
    });
  },
};