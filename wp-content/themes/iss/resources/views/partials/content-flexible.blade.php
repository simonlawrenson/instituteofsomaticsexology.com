<!-- ACF: Load flexible field template -->
<!-- ACF: Pass ACF data in to template -->
@if ( have_rows('flexible_content') )
  @while ( have_rows('flexible_content') ) @php( the_row() )
    @if ( get_row_layout() === 'button_links' )
      @include('partials.flexible-'. get_row_layout(), [
        'title'       => get_sub_field('button_title'),
        'button_left'   => get_sub_field('button_left'),
        'button_center' => get_sub_field('button_center'),
        'button_right'  => get_sub_field('button_right'),
      ])

    @elseif ( get_row_layout() === 'fixed_scroll' )
      @include('partials.flexible-'. get_row_layout(), [
        'columns'     => get_sub_field('fixed_scroll_columns')
      ])

    @elseif ( get_row_layout() === 'library' )
      @include('partials.flexible-'. get_row_layout(), [
        'library'   => get_sub_field('library')
      ])
      
    @elseif ( get_row_layout() === 'media_content' )
      @if ( get_sub_field('layout_type') === 'img_cover' )
        @include('partials.flexible-'. get_row_layout() .'-img_cover', [
          'layout_type'   => get_sub_field('layout_type'),
          'title_left'    => get_sub_field('media_content_title_left'),
          'title_center'  => get_sub_field('media_content_title_center'),
          'title_right'   => get_sub_field('media_content_title_right'),
          'content'       => get_sub_field('media_content_content'),
          'img'           => get_sub_field('media_content_img'),
        ])
      @elseif ( get_sub_field('layout_type') === 'img_align_mid' )
        @include('partials.flexible-'. get_row_layout() .'-img_align_mid', [
          'layout_type'   => get_sub_field('layout_type'),
          'title'         => get_sub_field('media_content_title'),
          'content'       => get_sub_field('media_content_content'),
          'img'           => get_sub_field('media_content_img'),
        ])
      @elseif ( get_sub_field('layout_type') === 'img_align_bot' )
        @include('partials.flexible-'. get_row_layout() .'-img_align_bot', [
          'layout_type'   => get_sub_field('layout_type'),
          'title'         => get_sub_field('media_content_title'),
          'content'       => get_sub_field('media_content_content'),
          'img'           => get_sub_field('media_content_img'),
        ])
      @endif
    @endif

  @endwhile
@endif