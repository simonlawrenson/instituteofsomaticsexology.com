// import {TweenMax, Linear, TimelineMax} from 'gsap/all';
// import	 { Linear } from 'gsap/all';
import ScrollMagic from 'ScrollMagic';
import 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';
import anime from 'animejs/lib/anime.es';

export default {
  init() {
    // JavaScript to be fired on the home page
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
    // Welcome message text reveal
    // Wrap every letter with a p tag in a span
    let textWrappers = document.querySelectorAll('.header__title p'),
    i;
    for (i = 0; i < textWrappers.length; ++i) {
      // eslint-disable-next-line quotes
      textWrappers[i].innerHTML = textWrappers[i].textContent.replace(/[-A-Za-z0-9!$#%^&–*@()_+|~=`{}:";'<>?,.]/g, "<span class='letter'>$&</span>");
    }

    anime.timeline({
      loop: false,
    })
    .add({
      targets: '.header__title p .letter',
      translateX: [40,0],
      translateZ: 0,
      opacity: [0,1],
      easing: 'linear',
      duration: 2000,
      delay: function(el, i) {
        return 3000 + 30 * i;
      },
    });
    // Intro text reveal
    // Wrap every letter with a p tag in a span
    textWrappers = document.querySelectorAll('.header__subtitle');
    for (i = 0; i < textWrappers.length; ++i) {
      // eslint-disable-next-line quotes
      textWrappers[i].innerHTML = textWrappers[i].textContent.replace(/[-A-Za-z0-9!$#%^&–*@()_+|~=`{}:";'<>?,.]/g, "<span class='letter'>$&</span>");
    }

    anime.timeline({
      loop: false,
    })
    .add({
      targets: '.header__subtitle .letter',
      translateX: [40,0],
      translateZ: 0,
      opacity: [0,1],
      easing: 'linear',
      duration: 2000,
      delay: function(el, i) {
        return 1000 + 30 * i;
      },
    });

    // init controller
		const controller = new ScrollMagic.Controller();
    
    // build scene
    new ScrollMagic.Scene({
      delay: 2000,
      triggerElement: '.wrap',
    })
    // trigger animation by adding a css class
    .setTween('.animate__logo-container', {delay: 2, className: '+=' + 'reveal'}) // the tween durtion can be omitted and defaults to 1
    .reverse(false)
    // .addIndicators() // add indicators (requires plugin)
    .addTo(controller);
  },
};
