// import external dependencies
import 'jquery';
// import then needed Font Awesome functionality
import { library, dom } from '@fortawesome/fontawesome-svg-core';

// import the Facebook and Twitter icons
import { faBars } from '@fortawesome/pro-regular-svg-icons';

// add the imported icons to the library
library.add( faBars );
// tell FontAwesome to watch the DOM and add the SVGs when it detects icon markup
dom.watch();

// Import Slick Carousel
import 'slick-carousel/slick/slick.min';

// Import everything from autoload
import './autoload/**/*'

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import faculty from './routes/faculty';
import pageLibraryData from './routes/library';
import archiveCoursesData from './routes/archive-courses-data';
import singleCourses from './routes/single-courses';

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
  // Faculty page
  faculty,
  // Library page
  pageLibraryData,
  // Archive Courses template.
  archiveCoursesData,
  // Single Courses template.
  singleCourses,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());
