export default {
  init() {
    // JavaScript to be fired on the single course template
  },
  finalize() {
    // JavaScript to be fired on the single course template, after page specific JS is fired
    // JavaScript to be fired when slider is active
    const testimonial_slider = $('.testimonials__slider-init');
    const testimonial_settings = {
      adaptiveHeight: true,
      arrows: false,
      appendDots: $('.testimonials__slider-init'),
      autoplay: false,
      dots: true,
      infinite: true,
      slidesToScroll: 1,
      slidesToShow: 1,
      speed: 500,
      variableWidth: false,
      responsive: [
        {
          breakpoint: 200,
          settings: 'unslick',
        },
      ],
    };
    testimonial_slider.slick(testimonial_settings);

    // reslick only if it's not slick()
    $(window).on('resize', function() {
      if ($(window).width() < 200) {
        if (testimonial_slider.hasClass('slick-initialized')) {
          testimonial_slider.slick('unslick');
        }
        return;
      }
      if (!testimonial_slider.hasClass('slick-initialized')) {
        return testimonial_slider.slick(testimonial_settings);
      }
    });
    
    // Expand location course information box
    $('.location__link').on('click', function(e) {
      e.preventDefault();
      if ($(window).width() > 991) {
        // Add class to not clicked location
        $('.location__container').not($(this).parents('.location__container')).toggleClass('location__hidden');
        // Add class to clicked location
        $(this).parents('.location__container').toggleClass('location__active');
        if( $(this).parents('.location__container').hasClass('location__active') ) {
          window.setTimeout( function() {
            $('.location__active .course-details__table').css({
              'display' : 'block',
            });
          }, 1900);
        } else {
          $('.course-details__table').css({
            'display' : 'none',
          });
        }
      }
    });
  },
};
