<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
    <!-- Add page load animation -->
    <!-- Todo: Replace this with AniLink in Phase 2 -->
    <svg id="fader"></svg>
    <script>
      fadeInPage();
      function fadeInPage() {
        if (!window.AnimationEvent) {
          return;
        }
        var fader = document.getElementById('fader');
        fader.classList.add('fade-out');
      }
      document.addEventListener('DOMContentLoaded', function() {
        if (!window.AnimationEvent) {
          return;
        }
        var anchors = document.getElementsByTagName('a');
      
        for (var idx=0; idx<anchors.length; idx+=1) {
          if (anchors[idx].hostname !== window.location.hostname || anchors[idx].pathname === window.location.pathname) {
            continue;
          }
          anchors[idx].addEventListener('click', function(event) {
            var fader = document.getElementById('fader'),
                anchor = event.currentTarget;
            
            var listener = function() {
                window.location = anchor.href;
                fader.removeEventListener('animationend', listener);
            };
            fader.addEventListener('animationend', listener);
            
            event.preventDefault();
            fader.classList.add('fade-in');
          });
        }
      });
      window.addEventListener('pageshow', function (event) {
        if (!event.persisted) {
          return;
        }
        var fader = document.getElementById('fader');
        fader.classList.remove('fade-in');
      });
    </script>
    @php do_action('get_header') @endphp
    @include('partials.header')
    <div class="wrap" role="document">
      <div class="content">
        <main class="main">
          @yield('content')
        </main>
        @if (App\display_sidebar())
          <aside class="sidebar">
            @include('partials.sidebar')
          </aside>
        @endif
      </div>
    </div>
    @php do_action('get_footer') @endphp
    @include('partials.footer')
    @php wp_footer() @endphp
    <script src="//instant.page/3.0.0" type="module" defer integrity="sha384-OeDn4XE77tdHo8pGtE1apMPmAipjoxUQ++eeJa6EtJCfHlvijigWiJpD7VDPWXV1"></script>
  </body>
</html>
<!--

Developed by: 
=============
  Simon Lawrenson
  🌏: https://lawrenson.dev
  📨: hello@lawrenson.dev
-->