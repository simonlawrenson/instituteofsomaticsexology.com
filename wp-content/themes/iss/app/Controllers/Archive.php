<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class Archive extends Controller
{
  /**
   * Populate and return the Testimonials slider on Course archive
   */
  public function testimonials() {

    // Initialize the return variable
    $return = '';

    /**
     * WP_Query
     */
    $testimonials = get_posts([
        'order'          => 'DESC',
        'orderby'        => 'menu_order',
        'post_type'      => 'testimonials',
        'posts_per_page' => 10,
    ]);

    return array_map(function ($post) {
      return [
        'content'   => apply_filters( 'the_content', $post->post_content ),
      ];
    }, $testimonials);
  }
}