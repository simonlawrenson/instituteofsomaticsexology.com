@if ( $title || $button_left || $button_center || $button_right )
	<div class="button-links__container block__container">
		<div class="container-fluid rc-container-fluid">
			<div class="row">
				@if ( $title )
					<div class="col-12">
						<h3 class="button-links__title h2 block__title">{{ $title }}</h3>
					</div> <!-- / .col-12 -->
				@endif
				@if ( $button_left || $button_center || $button_right )
					<div class="col-12 col-md-10 col-lg-8 mr-auto ml-auto">
						<div class="button-links__buttons d-sm-flex justify-content-sm-around">
							@if ( $button_left )
								<a href="{{ $button_left['url'] }}" title="{{ $button_left['title'] }}" class="btn btn__offw">{{ $button_left['title'] }}</a>
							@endif
							@if ( $button_center )
								<a href="{{ $button_center['url'] }}" title="{{ $button_center['title'] }}" class="btn">{{ $button_center['title'] }}</a>
							@endif
							@if ( $button_right )
								<a href="{{ $button_right['url'] }}" title="{{ $button_right['title'] }}" class="btn btn__offw">{{ $button_right['title'] }}</a>
							@endif
						</div> <!-- / .button-links__buttons -->
					</div> <!-- / .col-12 -->
				@endif
				</div> <!-- / .col-12 -->
			</div> <!-- / .row -->
		</div> <!-- / .container-fluid -->
	</div> <!-- / .button-links__container -->
@endif