<!-- Faculty layout -->
@if( $columns )
	<div class="fixed-scroller__container">
		<div class="fixed-scroller__container__inner">
			@php $total_cols = count($columns) @endphp
			@php $col_break  = $total_cols / 3 @endphp
			@php $i 				 = 1 @endphp
			@foreach( array_chunk($columns, $col_break) as $cols)
				<div class="fixed-scroller__panel__container fixed-scroller__panel__container-{{ $loop->iteration }}">
					@foreach($cols as $content)
						<div class="fixed-scroller__panel fixed-scroller__img-container fixed-scroller__img-container-{{ $i++ }}">
							<div style="background-image:url({{ $content['scroll_img']['url'] }});" class="fixed-column__img"></div>
						</div> <!-- / .fixed-scroller__panel -->
						<div class="fixed-scroller__panel fixed-scroller__content-container fixed-scroller__content-container-{{ $i++ }}">
							<div class="fixed-scroller__content d-flex flex-column">
								<h4 class="fixed-scroller__name">{{ $content['scroll_title'] }}</h4>
								<p class="fixed-scroller__subtitle">{{ $content['scroll_subtitle'] }}</p>
								<div class="fixed-scroller__info">
									{!!$content['scroll_content'] !!}
								</div> <!-- / .fixed-column__panel -->
							</div> <!-- / .fixed-column__content -->
						</div> <!-- / .fixed-scroller__panel -->
					@endforeach
				</div> <!-- / .scroller__panel__container -->
			@endforeach
		</div> <!-- / #fixed-scroller__container__inner -->
	</div> <!-- / .fixed-scroller__container -->
	<div class="responsive-faculty__container">
		@foreach( $columns as $cols)
			<div class="responsive-faculty__row-order responsive-faculty__row-order-{{ $cols['responsive_order'] }}">
				<div class="container-fluid">
					<div class="row align-items-stretch">
						<div class="col-12 col-md-6 responsive-faculty__container__inner">
							<img src="{{ $cols['scroll_img']['sizes']['fixed-scroller-image'] }}" alt="{{ $cols['scroll_img']['alt'] }}" title="{{ $cols['scroll_img']['title'] }}" class="responsive-faculty__img">
						</div> <!-- / .col-12 -->
						<div class="col-12 col-md-6 responsive-faculty__container__inner">
							<div class="responsive-faculty__content">
								<h4 class="responsive-faculty__name">{{ $cols['scroll_title'] }}</h4>
								<p class="responsive-faculty__subtitle">{{ $cols['scroll_subtitle'] }}</p>
								<div class="fixed-responsive-faculty__info">
									{!!$cols['scroll_content'] !!}
								</div>
							</div>
						</div> <!-- / .col-12 -->
					</div> <!-- / .row -->
				</div> <!-- / .container-fluid -->
			</div> <!-- / .responsive-faculty__row-order -->
		@endforeach
	</div> <!-- / .responsive-faculty__container -->
@endif